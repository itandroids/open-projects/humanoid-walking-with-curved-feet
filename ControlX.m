function [u predictedState predictedZMP ymax ymin]=ControlX(M, N, rho, interaction, T, amplitude, Tstep, state, zmp, startTime,varMax,varMin,displacement)


A=[1,T, (T*T)/2;
    0,1,T;
    0,0,1];
B=[(T*T*T)/6;
    (T*T)/2;
    T];

ref=zmp';

[KMPC phi predictedC, G,Hqp, Aqp]=MPCmethod(amplitude, T, startTime,N, A, B, interaction, M, rho,displacement);


f= phi *state;
u = KMPC*(ref-f);


ymax=varMax*ones(N,1)+ref;
ymin=varMin*ones(N,1)+ref;
bqp =[ ymax - f; f - ymin];


fqp = 2*G'*(f - ref);
options = optimset('Algorithm','interior-point-convex','Display','final');
u1 = quadprog(Hqp,fqp,Aqp,bqp,[],[],[],[],[],options);
u=u1(1);

%---------------------------------------------
predictedState= A*state+B*u;

predictedZMP=predictedC(1,:)*predictedState;