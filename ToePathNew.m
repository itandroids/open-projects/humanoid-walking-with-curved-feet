%-------ToePath-----------------------------------
function [pathX pathZ ]=ToePathNew(referenceSystem, ciclePct,angle)

[rToe rHeel backFoot frontFoot hAnkle]=Geometry();
foot=backFoot+frontFoot;
%-----------------------------------------------------------------
pointContact=angle*rToe +foot/2+referenceSystem(1);
thetaToe = atan(hAnkle/(frontFoot));
lToe = sqrt((frontFoot)^2+hAnkle^2);
thetaT = (abs(angle)+abs(thetaToe));
pathZ =rToe+lToe*sin(thetaT);
pathX = -(lToe*cos(thetaT)-pointContact);
%------------------------------------------------------------
