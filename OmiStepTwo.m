function [nextSteps tosoPositionOneStep  torsoPositionTwoSteps] = OmiStepTwo(torso, isLeftSwing, vr, Tstep, ySep, T, N1, support)
%Todo

numSteps = ceil(T*N1 / Tstep)+5;
nextSteps = zeros(3, numSteps);

torsoEnd = zeros(3, 1);
torsoTwoSteps = zeros(3, 1);

tosoPositionOneStep=[];
torsoPositionTwoSteps =[];

for i=1:numSteps
    % Todo: fix the code to allow omnidirecional step planning
    
    torsoEnd(1) = torso(1) + vr(1) * Tstep;
     torsoEnd(2) = torso(2) + vr(2) * Tstep;
    torsoTwoSteps(1) = torsoEnd(1) ;   
     torsoTwoSteps(2) = torsoEnd(2) ;  
    if isLeftSwing  
       
        if vr(2)>0            
           displacement = 2.0 * vr(2) * Tstep + 2.0 * ySep ;
            support(2)=support(2)+displacement;
        elseif vr(2)==0
            displacement=0;
            support(2)=ySep;
         else
           displacement = - 2.0 * ySep;
            support(2)=support(2)+displacement;
        end     
                
    else
      
        if vr(2)>0
           displacement = - 2.0 * ySep; 
            support(2)=support(2)+displacement;
       elseif vr(2)==0
            displacement=0;
            support(2)=-ySep;
        elseif vr(2)<0
           displacement = 2.0 * vr(2) * Tstep - 2.0 * ySep; 
            support(2)=support(2)+displacement;
        end 
        
    end
   
    nextSteps(:, i) = torsoEnd + [0; support(2); 0];
    isLeftSwing = ~isLeftSwing;
    torso = torsoEnd;
    tosoPositionOneStep=[tosoPositionOneStep torsoEnd ];
    torsoPositionTwoSteps =[ torsoPositionTwoSteps  torsoTwoSteps  ];
    
    
end

end

