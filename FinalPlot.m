
figure(20)
plot3(trajeto(1,:),trajeto2(1,:),zeros(1,length(trajeto2(1,:))),'r','LineWidth',2)
hold on
%plot3(trajeto(end-60,:),trajeto2(end-60,:),zeros(1,length(trajeto2(end-60,:))),'r','LineWidth',2)
hold on
plot3(exitZMPX(1,:),exitZMPY(1,:),zeros(1,length(exitZMPY(1,:))),'g','LineWidth',2)
hold on
plot3(interX,interY,zeros(1,length(interX)),'b','LineWidth',2)
hold on
%plot3(estadox(1,1:end), estadoy(1,1:end),zeros(1,length(estadoy(1,1:end))),'k')
hold on
%plot3(footPositionRightX(1:end),footPositionRightY(1:end),footPositionRightZ(1:end))
hold on
%plot3(footPositionLeftX(1:end),footPositionLeftY(1:end),footPositionLeftZ(1:end))
hold on
PlotFlatFoot( steps, footSize, ySep)
grid on
     set ( gca , 'FontSize', 14) ;
     set(0, 'DefaultFigureRenderer', 'painters')
          set(gca,'TickLabelInterpreter','latex');
          axis equal
          xlabel('x $(m)$', 'fontsize',14,'interpreter','latex')
          ylabel('y $(m)$', 'fontsize',14,'interpreter','latex')
          zlabel('z $(m)$', 'fontsize',14,'interpreter','latex')
          legend('ZMP reference','System output','CoM displacement','Interpreter','latex')
          xlim ( [ -0.1 0.9] ) 
          ylim ( [ -0.4 0.25 ] ) 
          zlim ( [ -0.01 0.15 ] )
%-----------------------------------------------------------
figure(30)
plot3(footPositionRightX(1:end),footPositionRightY(1:end),footPositionRightZ(1:end),'k','LineWidth',2)
hold on
plot3(footPositionLeftX(1:end),footPositionLeftY(1:end),footPositionLeftZ(1:end),'b','LineWidth',2)
grid on
     set ( gca , 'FontSize', 14) ;
     set(0, 'DefaultFigureRenderer', 'painters')
          set(gca,'TickLabelInterpreter','latex');
          axis equal
          xlabel('x $(m)$', 'fontsize',14,'interpreter','latex')
          ylabel('y $(m)$', 'fontsize',14,'interpreter','latex')
          zlabel('z $(m)$', 'fontsize',14,'interpreter','latex')
          legend({'Right ankle','Left ankle'},'Location','northwest','NumColumns',2,'Interpreter','latex')
          xlim ( [ -0.02 0.9] ) 
          ylim ( [ -0.2 0.0 ] ) 
          zlim ( [ 0.0 0.25 ] )

figure(40)
plot3(interX(:,1),interY(:,1),interZ(:,1),'k','LineWidth',2)
grid on
     set ( gca , 'FontSize', 14) ;
     set(0, 'DefaultFigureRenderer', 'painters')
          set(gca,'TickLabelInterpreter','latex');
          axis equal
          xlabel('x $(m)$', 'fontsize',14,'interpreter','latex')
          ylabel('y $(m)$', 'fontsize',14,'interpreter','latex')
          zlabel('z $(m)$', 'fontsize',14,'interpreter','latex')
          xlim ( [ -0.02 0.9] ) 
          ylim ( [ -0.2 0.0 ] ) 
          zlim ( [ 0.15 0.25 ] )

   time= linspace(0,(0.04*numberInteraction),400);   
   time=time';
  figure(41)
plot(time(:,1),interX(:,1),'b','LineWidth',2)
hold on
plot(time(:,1),interY(:,1),'r','LineWidth',2)
hold on
plot(time(:,1),interZ(:,1),'g','LineWidth',2)
grid on
     set ( gca , 'FontSize', 14) ;
     set(0, 'DefaultFigureRenderer', 'painters')
          set(gca,'TickLabelInterpreter','latex');
          axis equal
          xlabel('Time $(s)$', 'fontsize',14,'interpreter','latex')
          ylabel('Position $(m)$', 'fontsize',14,'interpreter','latex')
          legend({'x(m)','y(m)','z(m)'},'Location','northwest','NumColumns',3,'Interpreter','latex')
          %zlabel('z $(m)$', 'fontsize',14,'interpreter','latex')
          xlim ( [ -0.02 4] ) 
          ylim ( [ -0.8 0.9 ] ) 
         % zlim ( [ 0.18 0.22 ] )
