
clear
clc
close all
interaction = 1;
Q = 1;
R = 1e-11;
g = 9.81;
startTime = 0;
fds = 0.4;
T = 0.04;
ySep = 0.1;
N = 60;
M=N;
isLeftSwing = false;
footGround=true;
totalTimeSimulation=0;
initial=true;
torso=[0; -ySep; 0];
leftFoot = 0*torso + [0; 0*ySep; 0];
rightFoot = 0*torso + [0; -2*ySep; 0];
footSize=[0.1374;0.09;0];
varMaxX=0.04;varMinX=-0.04;varMaxY=0;varMinY=-0.2;
doubleSupportRatio= 0.2;
rho = R;
amplitude = 0.01;
displacement = 0.18;
stateY=[ -ySep,0, 0]';
stateX=zeros(3,1);
stateZ=0;
interpolationY=zeros(3,1);
interpolationX=zeros(3,1);
discreteStateZ=0;
uY=0;
uX=0;
legLength = 0.093;
thighLength = 0.093;
hipLength = -0.0757/2;
curveSep=(ySep-abs(hipLength))/2;
cicleTime=0;
interpolation=0;
interpolationA=zeros(3,3);
interpolationB=zeros(3,1);
interpolationTime=0.01;
ciclePct=0;
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
numberInteraction=100;
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
%<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
%---------------geometry foot------------------------------
rightAndLeftFoot.leftFoot=zeros(3,1);
rightAndLeftFoot.leftFootRotation =zeros(3,3);
rightAndLeftFoot.rightFoot = zeros(3,1);
rightAndLeftFoot.rightFootRotation =zeros(3,3);
Body.p = zeros(3,1);
Body.R =zeros(3,3);
ti=doubleSupportRatio/2; tf=(1-doubleSupportRatio/2);
tZero=0; tTotal=1;
Tstep=0.4;
zStep=0.12;
angleInicial=25; angleFinal=-25;vr=[0.2, 0, 0];
nPoints = 25;
[hAnkle ,rToe, frontFoot, lToe,backFoot,rHeel, lHeel,A, B,D,leftHeel,leftToe,rBack,rFront]=InitialConditions(nPoints);

%------------------------------------------------------------------------------
if(isLeftSwing == true)
    previousSupport = leftFoot;
    support = rightFoot;
    isRightSwing=false;
else
    
    previousSupport = rightFoot;
    support = leftFoot;
    isRightSwing=true;    
end

nextSteps = OmiStep(torso, isLeftSwing, vr, Tstep, ySep, T, N, support);
auxStep=nextSteps; 
nextStepsA=nextSteps(1,:);
nextStepsB=nextSteps(2,:);
coef=CubicCoefficients(ti,tf,tTotal,angleInicial,angleFinal);
footstepIndex=1;index=0; indexBool=true;
nextSupport=nextSteps(:,footstepIndex);

%--------------------------------
previousSupportI = previousSupport;
supportI = support;
nextSupportI=nextSupport;
%--------------------------------
estadox=[];
estadoy=[];
trajeto=[];
trajeto2=[];
interX=[];
interY=[];
interZ=[];
exitZMPX=[];exitZMPY=[]; exitymax=[];exitymin=[];exityymax=[];exityymin=[];
ctd=0;
footPositionLeftZ=[];footPositionLeftY=[];footPositionLeftX=[];
footPositionRightZ=[];footPositionRightY=[];footPositionRightX=[];
vetorCTD=[];steps=[];
steps=[steps nextSteps];   startTime1=0;
acumulateLeftAngle=[];acumulateRightAngle=[];
%--------------------------------------------------------------------------
for i= 1:numberInteraction 
    interaction=i;
    if (startTime1>(1*(Tstep/T)*T))
        
        startTime1=0;
        %----------------------------
        ciclePct=0;
        ctd=0;
        isRightSwing=~isRightSwing;
        
        index=index+1;
        previousSupport = support;
        support=nextSupport;
        nextSupport = auxStep(:,index+1);
        
    end
    
    %----------------------------------------------------------------------
    if (startTime>(2*(Tstep/T)*T))
        startTime=0;  
        %----------------
        previousSupportI =nextSteps(:,footstepIndex);
        supportI=nextSteps(:,footstepIndex+1);
        nextSupportI = nextSteps(:,footstepIndex+2);
        torso=[supportI(1);-ySep;0];
        nextSteps = OmiStepTwo(torso, isLeftSwing, vr, Tstep, ySep, T, N, supportI); 
      
    end
    
    %--------------------------------------------------------
    [zmpX zmpY] = ReferenceZMP(nextSteps ,supportI,previousSupportI,N,doubleSupportRatio,Tstep , startTime,T,footSize);   
    
    trajeto  = [trajeto  zmpX'];
    trajeto2 = [trajeto2 zmpY'];
    
    estadox = [estadox   stateX];
    estadoy = [estadoy   stateY];
    
    [uY predictedStateY predictedZMPY yymax yymin] = ControlY(M, N, rho, interaction, T, amplitude, Tstep, stateY, zmpY,  startTime,varMaxY,varMinY,displacement);
    [uX predictedStateX predictedZMPX ymax ymin] = ControlX(M, N, rho, interaction, T, amplitude, Tstep, stateX, zmpX,  startTime,varMaxX,varMinX,displacement);
    H(i)=-amplitude*cos(2*pi*2.5*startTime)+displacement;
    exitymax=[exitymax ymax];exitymin=[exitymin ymin]; 
    exityymax=[exityymax yymax];exityymin=[exityymin yymin];
   exitZMPX=[exitZMPX predictedZMPX];exitZMPY=[exitZMPY predictedZMPY];
  %------------------------------------------------------------------------
  interpolation=0;
  %------------------------------------------------------------
  while (0 <=interpolation && interpolation < (0.04))      
      
      %------------------------------------------------
      
      interpolationA=[1,interpolation, (interpolation*interpolation)/2;
          0,1,interpolation;
          0,0,1];
      
      
      interpolationB=[(interpolation*interpolation*interpolation)/6;
          (interpolation*interpolation)/2;
          interpolation];
      
      interpolationX = interpolationA*stateX + interpolationB*uX;
      interpolationY = interpolationA*stateY + interpolationB*uY;
      discreteStateZ = -amplitude * cos(2 * pi * 2.5 *(startTime+ interpolation))+displacement;
      
      interX=[interX; interpolationX(1,1)];
      interY=[interY; interpolationY(1,1)];
      interZ=[interZ;discreteStateZ];
      %------------------------------------------------------
      numDiscrete=T/interpolationTime;
      ciclePct=ctd/(numDiscrete*1*(Tstep/T)+numDiscrete-1);
      vetorCTD=[ vetorCTD  ciclePct];
      
      [rightAndLeftFoot]=KinematicNew(ciclePct,doubleSupportRatio,isRightSwing,coef,ti, tf,angleInicial,angleFinal,zStep,previousSupport,support,nextSupport,nPoints,curveSep);
      %---------------------------------------------
      footPositionLeftZ=[footPositionLeftZ rightAndLeftFoot.leftFoot(3)];
      footPositionLeftX=[footPositionLeftX rightAndLeftFoot.leftFoot(1)];
      footPositionLeftY=[footPositionLeftY rightAndLeftFoot.leftFoot(2)];
      footPositionRightZ=[footPositionRightZ rightAndLeftFoot.rightFoot(3)];
      footPositionRightX=[footPositionRightX rightAndLeftFoot.rightFoot(1)];
      footPositionRightY=[footPositionRightY rightAndLeftFoot.rightFoot(2)];
      %-------------------------------------------------
      leftFootIK.R=rightAndLeftFoot.leftFootRotation ; 
      leftFootIK.p=rightAndLeftFoot.leftFoot;
      rightFootIK.R=rightAndLeftFoot. rightFootRotation ;
      rightFootIK.p=rightAndLeftFoot. rightFoot;
      Body.p = [interpolationX(1,1);interpolationY(1,1); discreteStateZ];
      Body.R =Ry(0);
      leftAngles = IK(Body,D,A,B,leftFootIK);
      rightAngles = IK(Body,-D,A,B,rightFootIK);
      acumulateLeftAngle=[acumulateLeftAngle leftAngles];
      acumulateRightAngle=[acumulateRightAngle rightAngles];
      %--------------------------------------------------------------------      
      interpolation=interpolation+interpolationTime;
      ctd=ctd+1;
      
  end
  
  
  %------------------------------------------------------------------------
  stateY= predictedStateY;
  stateX= predictedStateX;  
  %------------------------------------------------------------------------
  startTime =startTime+T; startTime1 =startTime1+T;
  totalTimeSimulation=totalTimeSimulation+T;    
end






