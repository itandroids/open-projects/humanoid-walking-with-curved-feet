function [pathX ,pathZ ]=HeelPathNew(referenceSystem, ciclePct, angle)

[rToe rHeel backFoot frontFoot hAnkle]=Geometry();
foot=backFoot+frontFoot;
pointContact=-abs(angle)*rHeel-foot/2+referenceSystem(1);%CUIDADO
thetaHeel=atan(hAnkle/backFoot);
lHeel = sqrt((backFoot)^2+hAnkle^2);
thetaH = (abs(angle)+abs(thetaHeel));
pathZ =  rHeel+lHeel*abs(sin(thetaH));
pathX = (lHeel*cos(thetaH)+pointContact);

%------------------------------------------------------------


