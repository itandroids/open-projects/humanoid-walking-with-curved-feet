
function coef=CubicCoefficients(ti,tf,tTotal,angleInicial,angleFinal)
tiT=0; tfT=ti; tiS=ti-ti; 
tfS=tf-ti; tiH=tf-tf; tfH=tTotal-tf;
A=[  tiT^3 tiT^2 tiT 1 zeros(1,8);
     tfT^3 tfT^2 tfT 1 zeros(1,8);
     3*tiT^2 2*tiT 1 0 zeros(1,8); 
     3*tfT^2 2*tfT 1 0 zeros(1,8);
     zeros(1,4) tiS^3 tiS^2 tiS 1 zeros(1,4);
     zeros(1,4) tfS^3 tfS^2 tfS 1 zeros(1,4);
     zeros(1,4) 3*tiS^2 2*tiS 1 0 zeros(1,4); 
     zeros(1,4) 3*tfS^2 2*tfS 1 0 zeros(1,4);
     zeros(1,8) tiH^3 tiH^2 tiH 1 ;
     zeros(1,8) tfH^3 tfH^2 tfH 1 ;
     zeros(1,8) 3*tiH^2 2*tiH 1 0 ; 
     zeros(1,8) 3*tfH^2 2*tfH 1 0     
     ];
B=[0;angleInicial*pi/180;0;0;angleInicial*pi/180;angleFinal*pi/180;0;0;angleFinal*pi/180;0;0;0];
coef=inv(A)*B;

end