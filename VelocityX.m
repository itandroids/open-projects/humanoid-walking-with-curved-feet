function [vToeH, vHeelH]=VelocityX(thetaCubicPointToe, lToe,thetaCubicToe,thetaCubicPointHeel,lHeel,thetaCubicHeel)

 vHeelH=-(lHeel*thetaCubicPointHeel*sin(thetaCubicHeel));
  vToeH=lToe* thetaCubicPointToe*sin(thetaCubicToe);
 
end