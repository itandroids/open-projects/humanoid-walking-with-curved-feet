function [thetaCubicToe, thetaCubicHeel,thetaCubicPointToe,thetaCubicPointHeel]=AnglePoint(coef,ti,tf)
        
thetaCubicToe=coef(5)*(ti-ti)^3+coef(6)*(ti-ti)^2+coef(7)*(ti-ti)+coef(8);
thetaCubicHeel=coef(5)*(tf-ti)^3+coef(6)*(tf-ti)^2+coef(7)*(tf-ti)+coef(8);
thetaCubicPointToe=3*coef(5)*(ti-ti)^2+2*coef(6)*(ti-ti)+coef(7);
thetaCubicPointHeel=3*coef(5)*(tf-ti)^2+2*coef(6)*(tf-ti)+coef(7);
end