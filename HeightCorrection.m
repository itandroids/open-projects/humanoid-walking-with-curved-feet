function H=HeightCorrection(radius, hFoot, angle)

H=radius-abs(radius*sin(angle))+hFoot;

end