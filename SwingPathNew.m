%-----swing path

function [pathX ,pathZ ]= SwingPathNew(ti, tf,ciclePct,angleInicial,angleFinal,zStep,previousSupport,nextSupport,vToe,vHeel,vToeH, vHeelH)

xTrajectory=0.5*(1-cos(pi*(ciclePct-ti)/(tf-ti)));
[rToe rHeel backFoot frontFoot hAnkle]=Geometry();
foot=backFoot+frontFoot;
angleInicial=angleInicial*(pi/180);
angleFinal=angleFinal*(pi/180);
%-----------------------------------------------------------------
thetaToe=atan(hAnkle/frontFoot);
thetaT=(abs(angleInicial)+abs(thetaToe));
thetaHeel=atan(hAnkle/backFoot);
thetaH=(abs(angleFinal)+abs(thetaHeel));
%------------------------------------------------------------------
lToe=sqrt(frontFoot^2+hAnkle^2);
lHeel=sqrt(backFoot^2+hAnkle^2);
hToe=rToe+lToe*sin(thetaT);
hHeel=rHeel+lHeel*abs(sin(thetaH));

%--------------------------------------------------------------

vMidWay=0;
midWay=0.5;
%---------POLINOMIO grau 5----------------------------------
aS=[ ti^4 ti^3 ti^2 ti 1;
     tf^4 tf^3 tf^2 tf 1;
     midWay^4 midWay^3 midWay^2 midWay 1;
    4*ti^3 3*ti^2 2*ti 1 0;
     4*tf^3 3*tf^2 2*tf 1 0;
   ];
bS=[hToe;hHeel;zStep;vToe;vHeel];
X=inv(aS)*bS;

%-------transformation for pathX------------------------------------

pointInicial=-(lToe*cos(thetaT)-(angleInicial*rToe +foot/2+previousSupport(1)));
pointFinal=(lHeel*cos(thetaH)-abs(angleFinal)*rHeel-foot/2+nextSupport(1));
midWayX=pointInicial+(-pointInicial+pointFinal)/2;
aS1=[ ti^4 ti^3 ti^2 ti 1;
     tf^4 tf^3 tf^2 tf 1;
     midWay^4 midWay^3 midWay^2 midWay 1;
    4*ti^3 3*ti^2 2*ti 1 0;
     4*tf^3 3*tf^2 2*tf 1 0;
   ];
bS1=[pointInicial;pointFinal;midWayX;vToeH;vHeelH];
X1=inv(aS1)*bS1;


    
    pathX=X1(1)*ciclePct.^4+X1(2)*ciclePct.^3+X1(3)*ciclePct.^2+X1(4)*ciclePct+X1(5);
    pathZ=X(1)*ciclePct.^4+X(2)*ciclePct.^3+X(3)*ciclePct.^2+X(4)*ciclePct+X(5);
