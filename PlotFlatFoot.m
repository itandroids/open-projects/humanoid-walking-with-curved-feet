function PlotFlatFoot( steps, footSize,ySep)

length = footSize(1);% / 2;
width = footSize(2);% / 2;
leftFoot =  [0; 0*ySep; 0];% mudar a referencia
rightFoot = [0; -2*ySep; 0];
numSteps = size(steps, 2);
steps=[ leftFoot   steps];
str=["k"; "k"]; 
%figure;
hold on;
index=0;i=1;
for s=1:11%numSteps
    psi = steps(3, s);
    center = steps(1:2, s);
    R = rotationMatrix(psi);
    topLeft = center + R * [-length / 2; width / 2];
    topRight = center + R * [length / 2; width / 2];
    bottomLeft = center + R * [-length / 2; -width / 2];
    bottomRight = center + R * [length / 2; -width / 2];
    plotLine(topLeft, topRight, str(i));
    plotLine(topRight, bottomRight, str(i));
    plotLine(bottomRight, bottomLeft, str(i));
    plotLine(bottomLeft, topLeft, str(i));
    index=index+1;
    if index==2
        i=2;
    end
    if index==4
        i=1;index=0;
    end
end

    


end

function plotLine(point1, point2, color)

plot3([point1(1); point2(1)], [point1(2); point2(2)],[0.0 ;0.0] ,color);

end