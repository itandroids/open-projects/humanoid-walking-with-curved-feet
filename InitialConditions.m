
function [hAnkle ,rToe, frontFoot, lToe,backFoot,rHeel, lHeel,A, B,D,leftHeel,leftToe,rBack,rFront]=InitialConditions(nPoints)

hAnkle=0.0346;
rToe=0.01;
frontFoot=0.0717;
lToe=sqrt(hAnkle^2+frontFoot^2);
backFoot=0.0657;
rHeel=0.01;
lHeel=sqrt(hAnkle^2+backFoot^2);
A=0.093; B=0.093; D=0.037;
leftHeel= [-backFoot;0;-hAnkle];
leftToe= [frontFoot;0;-hAnkle];
rBack = linspace(pi, 3*pi/2, nPoints);
rFront= linspace(3*pi/2, 2*pi, nPoints);

end