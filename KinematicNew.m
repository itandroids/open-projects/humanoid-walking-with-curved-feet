function [rightAndLeftFoot]= KinematicNew(ciclePct,doubleSupportRatio,isRightSwing,coef,ti, tf,angleInicial,angleFinal,zStep,previousSupport,support,nextSupport,nPoints,curveSep)


rightAndLeftFoot.leftFoot=zeros(3,1);
rightAndLeftFoot.leftFootRotation =zeros(3,3);
rightAndLeftFoot.rightFoot = zeros(3,1);
rightAndLeftFoot.rightFootRotation =zeros(3,3);

[hAnkle ,rToe, frontFoot, lToe,backFoot,rHeel, lHeel,A, B,D,leftHeel,leftToe,rBack,rFront]=InitialConditions(nPoints);
footToAnkle=((frontFoot+backFoot)/2) -backFoot;
lHeel = sqrt((backFoot-rHeel)^2+hAnkle^2);
lToe = sqrt((frontFoot-rToe)^2+hAnkle^2);
  if(isRightSwing==true)
           
            %-----------------------------------------------------
            if (ciclePct <  doubleSupportRatio/2)
                thetaCubic=coef(1)*(ciclePct)^3+coef(2)*(ciclePct)^2+coef(3)*(ciclePct)+coef(4);
                
                [pathX ,pathZ ]= ToePathNew(previousSupport, ciclePct,thetaCubic);
                               
                rightAndLeftFoot.leftFoot = [(support(1)-abs(footToAnkle));
                    support(2)-curveSep;
                    (hAnkle+rToe)];
                rightAndLeftFoot.leftFootRotation = Ry(0);
                rightAndLeftFoot.rightFoot = [pathX;
                    nextSupport(2)+curveSep;
                    pathZ];
                rightAndLeftFoot.rightFootRotation = Ry(thetaCubic);
                
                
            end
            if (ciclePct>=doubleSupportRatio/2 && ciclePct<=(1-doubleSupportRatio/2))
                thetaCubic=coef(5)*(ciclePct-ti)^3+coef(6)*(ciclePct-ti)^2+coef(7)*(ciclePct-ti)+coef(8);
                
                %------------------derivada---------------------------------------
                [thetaCubicToe, thetaCubicHeel,thetaCubicPointToe,thetaCubicPointHeel]=AnglePoint(coef,ti,tf);
                %---------------condições  devido ao modulo  ----------------------
                [a,b,c,d]=Condiction(thetaCubicToe,(thetaCubicToe+atan(hAnkle/frontFoot)),thetaCubicHeel,(thetaCubicHeel+atan(hAnkle/backFoot)));
                vToe=Velocity(a,b,thetaCubicPointToe,rToe,lToe,thetaCubicToe,atan(hAnkle/frontFoot));
                vHeel=Velocity(c,d,thetaCubicPointHeel,rHeel,lHeel,thetaCubicHeel,atan(hAnkle/backFoot));
                [vToeH, vHeelH]=VelocityX(thetaCubicPointToe, lToe,thetaCubicToe,thetaCubicPointHeel,lHeel,thetaCubicHeel);
                
                %------------------------------------------------------------------
                [pathX ,pathZ]= SwingPathNew(ti, tf,ciclePct,angleInicial,angleFinal,zStep,previousSupport,nextSupport,vToe,vHeel,vToeH,vHeel);
                 rightAndLeftFoot.leftFoot = [(support(1)-abs(footToAnkle));
                    support(2)-curveSep;
                    (hAnkle+rToe)];
                rightAndLeftFoot.leftFootRotation = Ry(0);
                rightAndLeftFoot.rightFoot = [pathX;
                    nextSupport(2)+curveSep;
                    pathZ];
                rightAndLeftFoot.rightFootRotation = Ry(thetaCubic);
                
                
            end
            
            if (ciclePct>1-doubleSupportRatio/2)
                thetaCubic=coef(9)*(ciclePct-tf)^3+coef(10)*(ciclePct-tf)^2+coef(11)*(ciclePct-tf)+coef(12);
                [pathX ,pathZ]= HeelPathNew(nextSupport, ciclePct,thetaCubic);
                 rightAndLeftFoot.leftFoot = [(support(1)-abs(footToAnkle));
                    support(2)-curveSep;
                    (hAnkle+rToe)];
                rightAndLeftFoot.leftFootRotation = Ry(0);
                rightAndLeftFoot.rightFoot = [pathX;
                    nextSupport(2)+curveSep;
                    pathZ];
                rightAndLeftFoot.rightFootRotation = Ry(thetaCubic);
                
                
            end
            %-----------------------------------------------------------
        elseif(isRightSwing==false)
            
            if (ciclePct <  doubleSupportRatio/2)
                thetaCubic=coef(1)*(ciclePct)^3+coef(2)*(ciclePct)^2+coef(3)*(ciclePct)+coef(4);
                
                [pathX ,pathZ ]= ToePathNew(previousSupport, ciclePct,thetaCubic);
                               
                rightAndLeftFoot.leftFoot = [pathX;
                    nextSupport(2)-curveSep;
                    pathZ];
                rightAndLeftFoot.leftFootRotation = Ry(thetaCubic);
                rightAndLeftFoot.rightFoot = [(support(1)-abs(footToAnkle));
                    support(2)+curveSep;
                    (hAnkle+rToe)];
                rightAndLeftFoot.rightFootRotation = Ry(0);
                
                
            end
            if (ciclePct>=doubleSupportRatio/2 && ciclePct<=(1-doubleSupportRatio/2))
                thetaCubic=coef(5)*(ciclePct-ti)^3+coef(6)*(ciclePct-ti)^2+coef(7)*(ciclePct-ti)+coef(8);
                
                %------------------derivada---------------------------------------
                [thetaCubicToe, thetaCubicHeel,thetaCubicPointToe,thetaCubicPointHeel]=AnglePoint(coef,ti,tf);
                %---------------condições  devido ao modulo  ----------------------
                [a,b,c,d]=Condiction(thetaCubicToe,(thetaCubicToe+atan(hAnkle/frontFoot)),thetaCubicHeel,(thetaCubicHeel+atan(hAnkle/backFoot)));
                vToe=Velocity(a,b,thetaCubicPointToe,rToe,lToe,thetaCubicToe,atan(hAnkle/frontFoot));
                vHeel=Velocity(c,d,thetaCubicPointHeel,rHeel,lHeel,thetaCubicHeel,atan(hAnkle/backFoot));
                [vToeH, vHeelH]=VelocityX(thetaCubicPointToe, lToe,thetaCubicToe,thetaCubicPointHeel,lHeel,thetaCubicHeel);
                
                %------------------------------------------------------------------
                [pathX ,pathZ]= SwingPathNew(ti, tf,ciclePct,angleInicial,angleFinal,zStep, previousSupport,nextSupport,vToe,vHeel,vToeH,vHeelH);
               rightAndLeftFoot.leftFoot = [pathX;
                    nextSupport(2)-curveSep;
                    pathZ];
                rightAndLeftFoot.leftFootRotation = Ry(thetaCubic);
                rightAndLeftFoot.rightFoot = [(support(1)-abs(footToAnkle));
                    support(2)+curveSep;
                    (hAnkle+rToe)];
                rightAndLeftFoot.rightFootRotation = Ry(0);
                
            end
            
            if (ciclePct>1-doubleSupportRatio/2)
                thetaCubic=coef(9)*(ciclePct-tf)^3+coef(10)*(ciclePct-tf)^2+coef(11)*(ciclePct-tf)+coef(12);
                [pathX ,pathZ]= HeelPathNew(nextSupport, ciclePct,thetaCubic);
                rightAndLeftFoot.leftFoot = [pathX;
                    nextSupport(2)-curveSep;
                    pathZ];
                rightAndLeftFoot.leftFootRotation = Ry(thetaCubic);
                rightAndLeftFoot.rightFoot = [(support(1)-abs(footToAnkle));
                    support(2)+curveSep;
                    (hAnkle+rToe)];
                rightAndLeftFoot.rightFootRotation = Ry(0);
                
            end
  end