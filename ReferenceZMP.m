%function zmpX = ReferenceZMP(previousSupportI, support, nextSteps,...
    %walkTime,  doubleSupportRatio,  Tstep,  T,  N1,...
    %footSize)
 
function  [zmpTrajectoryX zmpTrajectoryY]= ReferenceZMP(footsteps,support,swingFoot,previewHorizonLength,doubleSupportRatio,period,walkTime,sampleTime,supportPolygon)
 %------------------condiçao o vetor tá vazio------------------------------
 %supportPolygon=[0.06;    0.03;    0.0];
 A=isempty(footsteps);
 
 if A==1
     disp('erro')
        support = [0.0  0.0];
         center = (support + swingFoot) / 2.0;
        for i=1:previewHorizonLength
            zmpTrajectoryX(i) = center(1);
            zmpTrajectoryY(i) = center(2);
        end
 end
 %------------------
  halfDoubleSupport = doubleSupportRatio * period / 2.0;
  singleSupportBegin = halfDoubleSupport;
  singleSupportEnd = period - halfDoubleSupport;
  footstepIndex = 0+1;
  numFootsteps = size(footsteps,2);
  %--------------------------------------------------------------
  
  for i=1:previewHorizonLength
        l=i;
        % Since the reference ZMP trajectory is a future reference,
        % we have to increment the walkTime first
        walkTime = walkTime +sampleTime;
        if (walkTime > period) 
            footstepIndex=footstepIndex+1;
            if (footstepIndex > numFootsteps)
                footstepIndex = numFootsteps;
                 
            end
            walkTime =walkTime - (period);
        end
        if (footstepIndex < numFootsteps) 
            
            if (footstepIndex == 0+1)
                previousSupport = swingFoot;
            elseif (footstepIndex == 1+1)
                previousSupport =support;
            else
                previousSupport = footsteps(:,footstepIndex - 2);
               
            end
            if (footstepIndex == 0+1)    
                    currentSupport =support;%[0.0 0.0 0.0]';
            else
                currentSupport = footsteps(:,footstepIndex - 1);
            end
                        
            
            nextSupport = footsteps(:,footstepIndex);
            centerBegin =(previousSupport + currentSupport) / 2.0; 
            centerEnd = (currentSupport + nextSupport) / 2.0;
            displacement = centerEnd - centerBegin;
            zmpSSBegin = centerBegin + displacement * (singleSupportBegin / period);
            zmpSSBegin=saturateZMP(zmpSSBegin, currentSupport, supportPolygon);
            zmpSSEnd = centerBegin + displacement * (singleSupportEnd / period);
            zmpSSEnd=saturateZMP(zmpSSEnd, currentSupport, supportPolygon);
            
            if (walkTime < singleSupportBegin) 
                 t = walkTime / singleSupportBegin;
                zmp = centerBegin * (1.0 - t) + zmpSSBegin * t;
                zmpTrajectoryX(1,i) = zmp(1);
                zmpTrajectoryY(1,i) = zmp(2);
               
            elseif (walkTime < singleSupportEnd) 
                 t = (walkTime - singleSupportBegin) / (singleSupportEnd - singleSupportBegin);
                zmp = zmpSSBegin * (1.0 - t) + zmpSSEnd * t;
                zmpTrajectoryX(1,i) = zmp(1);
                zmpTrajectoryY(1,i) = zmp(2);
                
             else 
                 t = (walkTime - singleSupportEnd) / (period - singleSupportEnd);
                zmp = zmpSSEnd * (1.0 - t) + centerEnd * t;            
                zmpTrajectoryX(1,i) = zmp(1);
                zmpTrajectoryY(1,i) = zmp(2);
                 
            end
            
            
        else
             currentSupport = footsteps(:,footstepIndex - 1);
            
            if (numFootsteps == 1)
                previousSupport = [0.0 0.0 0]';
            else
                previousSupport = footsteps(:,footstepIndex - 2);
             center = (previousSupport + currentSupport) / 2.0;
            zmpTrajectoryX(1,i) = center(1);
            zmpTrajectoryY(1,i) = center(2);
            end
        end
         %walkTime = walkTime +sampleTime;
  end
  
  %-----------função de saturação------------------------
 
function  ZMP=saturateZMP(zmp, currentSupport, supportPolygon)

    c=zmp;
    c1=currentSupport;
    zmp= zmp-currentSupport;
    
  if (zmp(1) < -1*supportPolygon(1) / 2.0)
        zmp(1) = -1*supportPolygon(1) / 2.0;
        
   elseif (zmp(1) > 1*supportPolygon(1) / 2.0) 
        zmp(1) =1*supportPolygon(1) / 2.0;
        
  end
    if (zmp(2) < -supportPolygon(2) /2 )
        zmp(2) = -supportPolygon(2) / 2;
    elseif (zmp(2) > supportPolygon(2) / 2.0)
        zmp(2) = supportPolygon(2) / 2.0;
    end

   zmp=zmp+currentSupport;
   ZMP=zmp;
end
%-------------------------------------------------------------------------
  
 end