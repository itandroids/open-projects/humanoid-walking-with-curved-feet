function [KMPC phi predictedC, G,Hqp, Aqp]=MPCmethod(amplitude, T, startTime,N, A, B, interaction, M, rho,displacement)


for k=1:N
g = 9.81;
startTime=startTime+T;      

H(k)=-amplitude*cos(2*pi*2.5*startTime)+displacement;
Ha(k)=amplitude*(2*pi*2.5)*(2*pi*2.5)*cos(2*pi*2.5*startTime);


Ct(k,:) = [1, 0, -H(k)/(Ha(k)+g)];
end

predictedC=Ct;

n = size(A,1);
At =A;
Bt =B;


j0=interaction-1;
G1 = zeros(N, N);

for i = 1:N
   for j = 1:N
       if i>j
            G1(j,i)=0;
       else
            G1(j,i) = (Ct(j,:))*(At^(j-i))*Bt;% Ct(j+j0,:)
       end
   end
end



G =G1;
%--------------------------------------------------------------------------
for i = 1:N
    phi(i,:) = Ct(i,:)*At^i;%Ct(i+j0,:)
    
end

aux2 = inv(G'*G + rho*eye(M)) * G';
KMPC =aux2(1,:);

Hqp = 2*(G'*G + rho*eye(M));

Aqp=[G;-G];

